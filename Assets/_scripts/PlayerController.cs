﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private static float inertia = 0.99F;
	private static float rotationInertia = 0.99F;

	public float maxRotationSpeed;

	public float maxSpeed;

	public float accel;
	public float rotAccel;

	[Range(0.0F, 1.0F)]
	public float forwardCorrection;

	[SerializeField]
	private Vector3 position{
		get { return this.transform.position; }
		set { this.transform.position = value; }
	}
	[SerializeField]
	private Vector3 velocity = Vector3.zero;

	private Quaternion rotation{
		get { return this.transform.localRotation; }
		set { this.transform.rotation = value; }
	}
	private Vector3 rotationVel = Vector3.zero;

	[SerializeField]
	private GameObject camRotate;
	[SerializeField]
	private GameObject cam;

	private Vector3 camOffset;

	[SerializeField]
	private GameObject bomb;

	private void Start(){

		camOffset = cam.transform.localPosition;
	}

	private float lastBomb = 1f;
	[SerializeField]
	private float bombTimeout = 3f;

	private void Update() {
		
		if(Input.GetAxis("Fire1") > 0){

			if(Time.time < lastBomb) return;

			Instantiate(bomb, this.transform.localPosition + 0.25f * -this.transform.up, Quaternion.identity);

			lastBomb = Time.time + bombTimeout;
		}
	}

	private void FixedUpdate() {
		
		velocity *= inertia;
		rotationVel *= rotationInertia;

		if(Mathf.Abs(rotationVel.x) < maxRotationSpeed)
			rotationVel.x += Input.GetAxis("Roll") * Time.deltaTime * rotAccel;
		
		if(Mathf.Abs(rotationVel.y) < maxRotationSpeed)
			rotationVel.y += Input.GetAxis("Yaw") * Time.deltaTime * rotAccel;
		
		if(Mathf.Abs(rotationVel.z) < maxRotationSpeed)
			rotationVel.z += Input.GetAxis("Pitch") * Time.deltaTime * rotAccel;

		if(this.velocity.magnitude < maxSpeed)
			this.velocity += Input.GetAxis("Thrust") * this.transform.forward * Time.deltaTime * accel;

		this.position += this.velocity * Time.deltaTime;

		this.transform.Rotate(Vector3.forward, rotationVel.x * Time.deltaTime, Space.Self);
		this.transform.Rotate(Vector3.up, rotationVel.y * Time.deltaTime, Space.Self);
		this.transform.Rotate(Vector3.left, rotationVel.z * Time.deltaTime, Space.Self);

		this.velocity = Vector3.LerpUnclamped(this.velocity, this.transform.forward * this.velocity.magnitude, forwardCorrection);

		this.position = new Vector3(
			Mathf.Clamp(this.position.x, -10F, 10F),
			Mathf.Clamp(this.position.y, -10F, 10F),
			Mathf.Clamp(this.position.z, -10F, 10F)
		);

		camRotate.transform.localRotation = Quaternion.Euler(new Vector3(this.rotationVel.z, this.rotationVel.y, this.rotationVel.x) / 25F);
		cam.transform.localPosition = (this.rotation * this.velocity) / 25F + camOffset;
	}
}
