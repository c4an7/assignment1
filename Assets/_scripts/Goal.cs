﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		
		if(other.gameObject.GetComponent<Physics.Sphere>() != null){

			GameObject.Destroy(other.gameObject);
		}	
	}
}
