﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics {

public class Controller : MonoBehaviour {

	[SerializeField]
	private float Gravity = 0.001f;

	public static Controller INSTANCE { get; private set; }

	private void Awake(){
		INSTANCE = this;
	}

	public static Vector3 parallel(Vector3 vector, Vector3 target){

		return Vector3.Dot(vector, target) * target;
	}

	public static Vector3 perpendicular(Vector3 vector, Vector3 target){

		return vector - parallel(vector, target);
	}

	public static float distToSurface(Vector3 p, Plane plane){

		float distance = parallel((p - plane.position), plane.normal).magnitude;

		if(Vector3.Dot(p - plane.position, plane.normal) >= 0){

			return distance;
		} else {
			return -distance;
		}
	}

	public static float distToSurface(Vector3 p, Sphere sphere){

		return (sphere.position - p).magnitude - sphere.radius;
	}

	private List<Sphere> spheres = new List<Sphere>();
	private List<Plane> planes = new List<Plane>();

	public void add(Sphere sphere){
		spheres.Add(sphere);
	}

	public void add(Plane plane){
		planes.Add(plane);
	}

	public static Vector3 getGravity(Sphere sphere){

		Vector3 g = new Vector3();

		foreach(Sphere s in INSTANCE.spheres){

			if(s == sphere) continue;

			float magnitude =
				(INSTANCE.Gravity * s.mass * sphere.mass) /
				Mathf.Pow(s.radius + sphere.radius, 2);
			
			g += (s.position - sphere.position).normalized * magnitude;
		}

		return g;
	}

	private void FixedUpdate() {
		
		for(int i = 0; i < spheres.Count; ++i){

			Sphere s = spheres[i];

			foreach(Plane p in planes){

				if(s.isColliding(p)){
					s.collide(p);
				}
			}

			for(int j = i + 1; j < spheres.Count; ++j){

				Sphere o = spheres[j];

				if(s.isColliding(o)){
					s.collide(o);
				}
			}
		}
	}
}

}
