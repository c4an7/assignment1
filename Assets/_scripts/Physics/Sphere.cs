﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics {

public class Sphere : MonoBehaviour {

	[SerializeField]
	private float restitution = 0.9f;
	[SerializeField]
	private float density = 1.0f;

	public Vector3 position {
		get { return this.transform.position; }
		set { this.transform.position = value; }
	}
	private Vector3 lastPosition = new Vector3();
	
	public Vector3 velocity = new Vector3();
	public Vector3 lastVelocity = new Vector3();

	public float radius {
		get { return this.transform.localScale.x / 2.0f; }
	}

	public float mass {
		get { return (4f/3f) * Mathf.PI * Mathf.Pow(this.radius, 3) * density; }
	}

	void Start () {
		Controller.INSTANCE.add(this);
	}

	private void FixedUpdate() {

		this.lastVelocity = this.velocity;
		this.lastPosition = this.position;

		this.velocity += Controller.getGravity(this) * Time.deltaTime;
		this.position += this.velocity * Time.deltaTime;	
	}

	public bool isColliding(Sphere sphere){

		return this.radius > Controller.distToSurface(this.position, sphere);
	}

	public bool isColliding(Plane plane){

		return this.radius > Controller.distToSurface(this.position, plane);
	}

	public void collide(Sphere sphere){

		Vector3 normal = (sphere.position - this.position).normalized;

		float diff = (this.radius + sphere.radius) - (sphere.position - this.position).magnitude;

		float offsetSelf = diff * (this.radius / (this.radius + sphere.radius));
		float offsetOther = diff * (sphere.radius / (this.radius + sphere.radius));

		this.position += offsetSelf * -normal;
		sphere.position += offsetOther * normal;

		Vector3 v1 = this.velocity;
		Vector3 v2 = sphere.velocity;

		Vector3 v1Perp = Controller.perpendicular(v1, normal);
		Vector3 v2Perp = Controller.perpendicular(v2, -normal);
		Vector3 v1Para = Controller.parallel(v1, normal);
		Vector3 v2Para = Controller.parallel(v2, -normal);

		Vector3 v1ParaNew = 
			(this.mass - sphere.mass)/(this.mass + sphere.mass) * v1Para +
			(2 * sphere.mass)/(this.mass + sphere.mass) * v2Para;
		
		Vector3 v2ParaNew =
			(this.mass - sphere.mass)/(this.mass + sphere.mass) * v2Para +
			(2 * this.mass)/(this.mass + sphere.mass) * v1Para;
		
		v1 = v1Perp + v1ParaNew * this.restitution;
		v2 = v2Perp + v2ParaNew * sphere.restitution;

		this.velocity = v1;
		sphere.velocity = v2;
	}

	public void collide(Plane plane){

		float d1 = Controller.distToSurface(this.lastPosition, plane) - this.radius;
		float d2 = Controller.distToSurface(this.position, plane) - this.radius;

		float dt = Time.deltaTime * (d1 / (d1 + d2));

		Vector3 v = this.velocity + Controller.getGravity(this) * dt;
		Vector3 p = this.position + this.velocity * dt;

		Vector3 paraVel = Controller.parallel(v, plane.normal);
		Vector3 perpVel = Controller.perpendicular(v, plane.normal);

		this.velocity = perpVel + (-paraVel * this.restitution);
		this.position = p;

		this.velocity += Controller.getGravity(this) * (Time.deltaTime - dt);
		this.position += this.velocity * (Time.deltaTime - dt);
	}
}

}
