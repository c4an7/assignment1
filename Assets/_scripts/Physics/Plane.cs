﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Physics {

public class Plane : MonoBehaviour {

	public Vector3 position {
		get { return this.transform.position; }
	}

	public Vector3 normal {
		get { return this.transform.up; }
	}

	public Vector3 scale {
		get { return this.transform.localScale; }
	}

	void Start () {

		Controller.INSTANCE.add(this);
	}
}

}
