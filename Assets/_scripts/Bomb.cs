﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour {

	public float timer;
	public float timeInterval;

	public Color offColor;
	public Color onColor;

	public float explodeForce;
	public float explodeRadius;

	private void Awake() {

		StartCoroutine(explode());
	}

	private IEnumerator explode(){

		float t = 0f;

		bool colorOn = false;

		while(t < timer){

			t += timeInterval;

			if(colorOn){
				colorOn = false;
				this.GetComponent<Renderer>().material.color = offColor;
			} else {
				colorOn = true;
				this.GetComponent<Renderer>().material.color = onColor;
			}

			yield return new WaitForSeconds(timeInterval);
		}

		Collider[] colliders = UnityEngine.Physics.OverlapSphere(
			this.transform.position, explodeRadius
		);

		foreach(Collider collider in colliders){

			Physics.Sphere sphere = collider.GetComponent<Physics.Sphere>();

			if(sphere == null) continue;

			Vector3 dist = sphere.position - this.transform.position;

			float force = explodeForce / (dist.magnitude * Mathf.Pow(sphere.mass, 2));

			sphere.velocity += dist.normalized * force;
		}

		Destroy(this.gameObject);
	}
}
