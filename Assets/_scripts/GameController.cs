﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	[SerializeField]
	private GameObject physics_sphere;

	public int num_spheres = 50;

	public Vector3 boundsMin = new Vector3(-10, -10, -10);
	public Vector3 boundsMax = new Vector3(10, 10, 10);

	public float randomVelocityStrength = 1.0F;

	private void Start() {
		
		for(int i = 0; i < num_spheres; ++i){

			Vector3 pos;

			do {

				pos = new Vector3(
					Random.Range(boundsMin.x, boundsMax.x),
					Random.Range(boundsMin.y, boundsMax.y),
					Random.Range(boundsMin.z, boundsMax.z)
				);

			} while(UnityEngine.Physics.OverlapSphere(pos, 1).Length > 0);

			Quaternion rot = Quaternion.Euler(
				Random.Range(180, -180),
				Random.Range(180, -180),
				Random.Range(180, -180)
			);

			Vector3 vel = rot * Vector3.up * randomVelocityStrength;

			GameObject sphere = Instantiate(physics_sphere, pos, Quaternion.identity);

			sphere.GetComponent<Physics.Sphere>().velocity = vel;
		}
	}
}
