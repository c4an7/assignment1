﻿Shader "Unlit/bounds"
{
	Properties {
		_distance ("Focus Distance", Float) = 5
		_color ("Color", Color) = (0, 0, 0) 
		_scale ("UV Scale", Float) = 1
	}

	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		LOD 100

		ZWrite Off
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass {

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata {

				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {

				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float dist : TEXCOORD1;
			};
			
			float _distance;
			float4 _color;
			float _scale;

			v2f vert (appdata v){
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.dist = length(WorldSpaceViewDir(v.vertex) / _distance);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target {

				float4 col = _color;

				col.a *= (i.uv.x + i.uv.y);

				col.a = (col.a * _scale) % 1;

				col.a *= 1/(pow(i.dist, 10)+1);

				return col;
			}
			ENDCG
		}
	}
}
